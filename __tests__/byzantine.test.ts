import {
  poolAddressEndpoint,
  seedEndpoint,
  comparePoolAddresses,
  sortPoolAddressByChain,
  comparePoolAddress,
  comparePoolAddressLists,
  toPoolAddressList,
  baseUrl,
  getPoolConsensus,
  getFaultTolerance,
  getPoolChecks,
  MAX_POOL_CHECKS,
  midgartBaseUrl,
  resetPoolChecks
} from '../src/byzantine'
import { axiosMock, mockThorchainEndpoint, mockThorchainEndpoints, mockPoolAddress, mockPoolAddresses } from './mocks'

describe('byzantine', () => {
  const ip1 = '121.0.0.1'
  const ip2 = '121.0.0.2'
  const ip3 = '121.0.0.3'
  const ipOff1 = '121.0.0.11'
  const ipOff2 = '121.0.0.12'
  const ip404 = '121.0.0.20'
  const mockTE1 = mockThorchainEndpoint({ chain: 'C1', address: 'address1' })
  afterAll(() => {
    axiosMock.restore()
  })
  beforeAll(() => {
    axiosMock.onGet(poolAddressEndpoint(ip1)).reply(
      200,
      mockThorchainEndpoints({
        current: [mockTE1]
      })
    )
    axiosMock.onGet(poolAddressEndpoint(ip2)).reply(
      200,
      mockThorchainEndpoints({
        current: [mockTE1]
      })
    )
    axiosMock.onGet(poolAddressEndpoint(ip3)).reply(
      200,
      mockThorchainEndpoints({
        current: [mockTE1]
      })
    )
    axiosMock.onGet(poolAddressEndpoint(ipOff1)).timeout()
    axiosMock.onGet(poolAddressEndpoint(ipOff2)).timeout()
    axiosMock.onGet(poolAddressEndpoint(ip404)).reply(404)
  })
  beforeEach(() => {
    resetPoolChecks()
  })
  describe('getFaultTolerance', () => {
    it('from 3', () => {
      expect(getFaultTolerance(3)).toEqual(2)
    })
    it('from 2', () => {
      expect(getFaultTolerance(2)).toEqual(2)
    })
    it('from 1', () => {
      expect(getFaultTolerance(1)).toEqual(1)
    })
    it('from 10', () => {
      expect(getFaultTolerance(10)).toEqual(4)
    })
    it('from 100', () => {
      expect(getFaultTolerance(100)).toEqual(34)
    })
  })

  describe('seedEndpoint', () => {
    it('returns endpoint for testnet by default', () => {
      expect(seedEndpoint('testnet')).toEqual('https://testnet-seed.thorchain.info/node_ip_list.json')
    })
    it('returns endpoint for chaosnet', () => {
      expect(seedEndpoint('chaosnet')).toEqual('https://chaosnet-seed.thorchain.info/node_ip_list.json')
    })
    it('returns endpoint for mainnet', () => {
      expect(seedEndpoint('mainnet')).toEqual('https://seed.thorchain.info/node_ip_list.json')
    })
  })

  describe('poolAddressEndpoint', () => {
    it('returns endpoint to fetch pool addresses by given ip', () => {
      expect(poolAddressEndpoint('127.0.0.1')).toEqual('http://127.0.0.1:8080/v1/thorchain/pool_addresses')
    })
  })

  describe('toPoolAddressList', () => {
    it('converts PoolAddresses[] into PoolAddress[]', () => {
      const a1 = mockPoolAddress()
      const a2 = mockPoolAddress()
      const a3 = mockPoolAddress()
      const list = [mockPoolAddresses({ addresses: [a1, a2] }), mockPoolAddresses({ addresses: [a3] })]
      const expected = [[a1, a2], [a3]]
      expect(toPoolAddressList(list)).toEqual(expected)
    })
  })

  describe('sortPoolAddressByChain', () => {
    it('sorts pool addresses', () => {
      const a1 = mockPoolAddress({ chain: 'A' })
      const a2 = mockPoolAddress({ chain: 'B' })
      const a3 = mockPoolAddress({ chain: 'C' })
      const addresses = [a2, a3, a1]
      const expected = [a1, a2, a3]
      expect(addresses.sort(sortPoolAddressByChain)).toEqual(expected)
    })
  })

  describe('comparePoolAddress', () => {
    it('returns true if addresses are equal', () => {
      const a1 = mockPoolAddress()
      expect(comparePoolAddress(a1, a1)).toBeTruthy()
    })
    it('returns false if chains are not equal', () => {
      const a1 = mockPoolAddress({ chain: 'a' })
      const a2 = mockPoolAddress({ chain: 'b' })
      expect(comparePoolAddress(a1, a2)).toBeFalsy()
    })
    it('returns false if addresses are not equal', () => {
      const a1 = mockPoolAddress({ address: 'a' })
      const a2 = mockPoolAddress({ address: 'b' })
      expect(comparePoolAddress(a1, a2)).toBeFalsy()
    })
  })

  describe('comparePoolAddressLists', () => {
    it('returns true if lists are equal', () => {
      const a1 = mockPoolAddress()
      const a2 = mockPoolAddress()
      const a3 = mockPoolAddress()
      const A1 = [a1, a2, a3]
      const A2 = [a1, a2, a3]
      expect(comparePoolAddressLists(A1, A2)).toBeTruthy()
    })
    it('returns false if lists includes different pool addresses', () => {
      const a1 = mockPoolAddress()
      const a2 = mockPoolAddress()
      const a3 = mockPoolAddress()
      const A1 = [a2, a3]
      const A2 = [a1, a2]
      expect(comparePoolAddressLists(A1, A2)).toBeFalsy()
    })
    it('returns false if length of lists are not equal', () => {
      const a1 = mockPoolAddress()
      const a2 = mockPoolAddress()
      const A1 = [a1]
      const A2 = [a1, a2]
      expect(comparePoolAddressLists(A1, A2)).toBeFalsy()
    })
    it('returns false if length of lists are not equal', () => {
      const a1 = mockPoolAddress()
      const a2 = mockPoolAddress()
      const A1 = [a1]
      const A2 = [a2]
      expect(comparePoolAddressLists(A1, A2)).toBeFalsy()
    })
  })

  describe('comparePoolAddresses', () => {
    it('returns false if pools are not equal', () => {
      const mockPA1 = mockPoolAddress()
      const mockPA2 = mockPoolAddress()
      const mockPA3 = mockPoolAddress()
      const poolsAddresses = [
        { ip: '127.0.0.1', addresses: [mockPA1, mockPA2] },
        { ip: '127.0.0.2', addresses: [mockPA1, mockPA2] },
        // bad actor
        { ip: '127.0.0.3', addresses: [mockPA1, mockPA3] }
      ]
      expect(comparePoolAddresses(poolsAddresses)).toBeFalsy()
    })
    it('returns true if all pools are equal', () => {
      const mockPA1 = mockPoolAddress()
      const poolsAddresses = [
        { ip: '127.0.0.1', addresses: [mockPA1] },
        { ip: '127.0.0.2', addresses: [mockPA1] },
        { ip: '127.0.0.3', addresses: [mockPA1] }
      ]
      expect(comparePoolAddresses(poolsAddresses)).toBeTruthy()
    })
  })

  describe('getPoolConsensus', () => {
    it('returns a random list of IPs if a BFT check succeeded', async () => {
      // Since `getPoolConsensus` returns a random list of IPs,
      // we can just expect it returns an array and its length
      const result = await getPoolConsensus([ip1, ip2, ip3])
      expect(result).toEqual(expect.any(Array))
      expect(result.length).toEqual(2)
    })
    it('returns same, but shuffled list of IPs, if two nodes are active only', async () => {
      // Since `getPoolConsensus` returns a random list of IPs,
      // we can just expect it returns an array and its length
      const result = await getPoolConsensus([ip1, ip2])
      expect(result).toEqual(expect.any(Array))
      expect(result.length).toEqual(2)
    })
    it('returns same list of IPs if one node is active only', async () => {
      // Since `getPoolConsensus` returns a random list of IPs,
      // we can just expect it returns an array and its length
      const result = await getPoolConsensus([ip1])
      expect(result).toEqual([ip1])
    })
    it('returns an error if BFT cant be reached due offline nodes', async () => {
      await expect(getPoolConsensus([ipOff1, ip1, ipOff1])).rejects.toThrow()
      // reaches max. checks
      expect(getPoolChecks()).toEqual(MAX_POOL_CHECKS)
    })
    it('returns an error if BFT cant be reached due a 404 nodes', async () => {
      await expect(getPoolConsensus([ip1, ip404, ip404])).rejects.toThrow()
      // reaches max. checks
      expect(getPoolChecks()).toEqual(MAX_POOL_CHECKS)
    })
  })

  describe('baseUrl', () => {
    it('returns a base url', async () => {
      axiosMock.onGet(seedEndpoint('mainnet')).reply(200, [ip1, ip2, ip3])
      // Since `baseUrl` returns a random base url,
      // we can just test the beginning of the returned base path here
      await expect(baseUrl('mainnet', true)).resolves.toEqual(expect.stringContaining('http://121.0.0'))
    })
    it('returns a base url having two nodes running', async () => {
      axiosMock.onGet(seedEndpoint('mainnet')).reply(200, [ip1, ip2])
      // Since `baseUrl` returns a random base url,
      // we can just test the beginning of the returned base path here
      await expect(baseUrl('mainnet', true)).resolves.toEqual(expect.stringContaining('http://121.0.0'))
    })
    it('returns a base url having one node only', async () => {
      axiosMock.onGet(seedEndpoint('mainnet')).reply(200, [ip1])
      await expect(baseUrl('mainnet', true)).resolves.toEqual(midgartBaseUrl(ip1))
    })
    it('rejects if most of nodes are not available', async () => {
      axiosMock.onGet(seedEndpoint('mainnet')).reply(200, [ipOff1, ipOff2, ip3])
      await expect(baseUrl('mainnet', true)).rejects.toThrow()
      // reaches max. checks
      expect(getPoolChecks()).toEqual(MAX_POOL_CHECKS)
    })
  })
})
