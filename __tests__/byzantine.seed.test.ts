import { getIpList, seedEndpoint } from '../src/byzantine'
import { axiosMock } from './mocks'

describe('byzantine', () => {
  describe('getIpList', () => {
    const ip1 = '121.0.0.1'
    const ip2 = '121.0.0.2'
    const ip3 = '121.0.0.4'

    afterAll(() => {
      axiosMock.restore()
    })

    it('returns a valid list', async () => {
      axiosMock.onGet(seedEndpoint('mainnet')).reply(200, [ip1, ip2, ip3])
      const exected = [ip1, ip2, ip3]
      await expect(getIpList('mainnet')).resolves.toEqual(exected)
    })
    it('throws an error if list is empty', async () => {
      axiosMock.onGet(seedEndpoint('mainnet')).reply(200, [])
      await expect(getIpList('mainnet')).rejects.toThrow()
    })
    it('throws an error endpoint is not accessible', async () => {
      axiosMock.onGet(seedEndpoint('mainnet')).reply(500)
      await expect(getIpList('mainnet')).rejects.toThrow()
    })
    it('it throws an error if endpoint is not available', async () => {
      axiosMock.onGet(seedEndpoint('mainnet')).timeoutOnce()
      await expect(getIpList('mainnet')).rejects.toThrow()
    })
  })
})
