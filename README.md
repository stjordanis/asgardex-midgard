# ASGARDEX-MIDGARD

The ASGARDEX Midgard package is a Midgard verification module used by `ASGARDEX` clients. It provides an easy way to get a valid, but random base url of [Midgards API](https://gitlab.com/thorchain/midgard/) by proofing active [THORNodes](https://gitlab.com/thorchain/thornode).

It will do all the hard work behind the scenes by challenging active THORNodes. If Midgard cannot get consensus on data, it will blacklist bad nodes and try again. This is because of THORChain's trust model - it does not trust any Node, instead it verifies them by comparing against each other. It assumes that no more than 1/3rd of THORNodes are malicious and colluding.

Midgard gets the list of active nodes by querying the seed service: `testnet-seed.thorchain.info` (testnet), `chaosnet-seed.thorchain.info` (chaosnet) or `seed.thorchain.info` (mainnet), which crawls THORChain for active node IPs.

Note: The Seed Service is now the weak link and must be semi-trusted (however it can be independently verified by auditing the nodes manually). In future the Seed Service will move to an Ethereum Smart Contract and Midgard will use Web3 to retrieve seed nodes IPs. This will allow the service to be fully trustless.

## Installation

- Install `@thorchain/asgardex-midgard` from `npm`

```bash
yarn add @thorchain/asgardex-midgard
```

- Note: If [your target browser does not support `Promise.allSettled`](https://caniuse.com/?search=Promise.allSettled), you will find a polyfill [here](https://github.com/amrayn/allsettled-polyfill)

## Usage

### Basic usage

Whenever your application needs to ask Midgard API for data, call `midgard()` before. It will return a valid, but random base url for Midgard API.

**Please note:** That's the only way to send a transaction to a proofed THORNode at any time.

```js
import midgard from '@thorchain/asgardex-midgard'

// baseUrl on testnet
const baseUrl = await midgard()
// or
const baseUrl = await midgard('testnet')
//
// baseUrl on chaosnet
const baseUrl = await midgard('chaosnet')
//
// baseUrl on mainnet
const baseUrl = await midgard('mainnet')

// fetch data from an endpoint, for example to get data of `/v1/pools`
const data = await fetch(`${baseUrl}/v1/pools`)
```

**Side note**: Behind the scenes `Midgard` is memorizing a proofed list of `baseUrl`s to avoid increasing requests. `Midgard` will recycle its cache every hour. If you want to get a "fresh", not cached `baseUrl`, set the second parameter to `true`.

```js
// Testnet example:
const baseUrl = await midgard('testnet', true)
// Chaosnet example:
const baseUrl = await midgard('chaosnet', true)
// Mainnet example:
const baseUrl = await midgard('mainnet', true)
```

### Examples

#### Testnet

```bash
ts-node examples/testnet.ts
# output
testnet: http://136.243.227.164:8080
```

#### Chaosnet

```bash
ts-node examples/chaosnet.ts
# output
chaosnet: http://54.255.107.211:8080
```

#### Mainnet

```bash
ts-node examples/mainnet.ts
# output an error (as long as mainnet is not supported)
Error: Empty IP list: [object Object]
```

That might be helpful for error handling, see next chapter "Error handling".

### Error handling

In same cases a cached `baseUrl` can be invalid if a node goes offline for any reason. In this case you can force `Midgard` to return a non-cached `baseUrl` to retry this request, but using another `baseUrl`.

```js
// Get cached `baseUrl` as usual
// Testnet example:
const baseUrl = await midgard('testnet')

// Catch errors to retry another request using a "fresh" proofed `baseUrl`
try {
  const data = await fetch(`${baseUrl}/v1/pools`)
  ...
} catch (error) {
  retry()
}

const retry = () => {
  // Get another, "fresh" proofed `baseUrl`
  const anotherBaseUrl = await midgard('testnet', true)
  const data = await fetch(`${baseUrl}/v1/pools`)
}

```

**Please note:** Use a non-cached `baseUrl` as few as possible to avoid to increase requests made by `Midgard`.

## Development

### Build

```bash
yarn build
```

### Tests

```bash
yarn test
```
