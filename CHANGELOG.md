# Changelog

## 1.1.0 (2020-10-21)

- Fix consensus in case of unreachable nodes
- Update to Node 14
- Update all dependencies
- Add (simple) examples

## 1.0.0 (2020-08-27)

- Support endpoint for `chaosnet`

### BREAḰING CHANGE:

- `Network` is used for `midgard()` to parameterize `testnet`, `chaosnet` or `mainnet`, for example `await midgard('chaosnet')`

## 0.1.0 (2020-06-05)

### Initial release
