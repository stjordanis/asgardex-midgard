import axios from 'axios'
import _ from 'lodash'
import store from 'store'
import { Network } from './types'

/**
 * Midgard type for Thorchain endpoint
 */
export type ThorchainEndpoint = {
  address?: string
  chain?: string
  pubKey?: string
}

/**
 * Midgard type for Thorchain endpoints
 */
export type ThorchainEndpoints = {
  current?: Array<ThorchainEndpoint>
}

/**
 * IP type
 */
export type IP = string

/**
 * IP list
 */
export type IPList = IP[]

/**
 * Internal state for a list of cached IPs
 *
 */
let cachedIpList: IPList = []

/**
 * Max. number of pool checks
 *
 * Note: It's been exported for running tests only,
 * it will never be exported (public) in `index.ts`
 */
export const MAX_POOL_CHECKS = 5

/**
 * Counter of pool checks
 */
let poolChecks = 0

/**
 * Getter for `poolChecks` - used by tests only
 */
export const getPoolChecks = () => poolChecks

/**
 * Resets `poolChecks` - used by tests only
 */
export const resetPoolChecks = () => (poolChecks = 0)

/**
 * Default header for Axios requests
 */
const getDefaultHeader = () => ({
  'Content-Type': 'application/json'
})

/**
 * Calculates fault tolerance by a given number
 *
 * @param max - Maximum number to get a fault tolerance from
 *
 * @returns Fault tolerance
 */
export const getFaultTolerance = (max: number) => (max >= 3 ? Math.floor(max / 3) + 1 : max)

/**
 * Helper to get endpoint for loading IP list of current active nodes
 *
 * @param network - Network
 *
 * @returns Seed endpoint (default: endpoint for testnet)
 */
export const seedEndpoint = (network: Network) => {
  const endpoint = (sub: string) => `https://${sub}.thorchain.info/node_ip_list.json`
  if (network === 'chaosnet') return endpoint('chaosnet-seed')
  if (network === 'mainnet') return endpoint('seed')
  // testnet by default
  return endpoint('testnet-seed')
}

/**
 * Returns a base url of Midgard API
 *
 * @param ip - IP of a node
 *
 * @returns Midgard base url
 */
export const midgartBaseUrl = (ip: IP) => `http://${ip}:8080`

/**
 * Returns an endpoint to request pool addresses
 *
 * @param ip - IP of a node
 *
 * @returns Endpoint
 */
export const poolAddressEndpoint = (ip: IP) => `${midgartBaseUrl(ip)}/v1/thorchain/pool_addresses`

export type PoolAddress = Pick<ThorchainEndpoint, 'address' | 'chain'>
export type PoolAddresses = {
  ip: IP
  addresses: PoolAddress[]
}
export type PoolAddressMap = Map<IP, PoolAddress[]>

/**
 * Helper to compare two `PoolAddress`'es
 *
 * @param a - `PoolAddress`
 * @param b - `PoolAddress`
 *
 * @returns Equity as boolean
 */
export const comparePoolAddress = (a: PoolAddress, b: PoolAddress) => {
  if (a.address && b.address && a.chain && b.chain) {
    return a.address === b.address && a.chain === b.chain
  }
  return false
}

/**
 * Helper to compare two lists of `PoolAddress`'es
 *
 * @param a - List of `PoolAddress`
 * @param b - Another list of `PoolAddress`
 *
 * @returns Equity as boolean
 */
export const comparePoolAddressLists = (a: PoolAddress[], b: PoolAddress[]) => {
  if (a.length !== b.length) return false
  // filter different addresses
  const differences = a.filter((pa: PoolAddress) => !b.some((pb: PoolAddress) => comparePoolAddress(pa, pb)))
  return !differences.length
}

/**
 * Helper to sort two `PoolAddress`'es by its `chain`
 *
 * @param a - `PoolAddress`
 * @param b - `PoolAddress`
 *
 * @returns Sort number
 */
export const sortPoolAddressByChain = (a: PoolAddress, b: PoolAddress) => {
  if (a.chain && b.chain) {
    return a.chain > b.chain ? 1 : -1
  }
  return 0
}

/**
 * Helper to transform `ThorchainEndpoint` to `PoolAddress`
 *
 * @param endpoint - `ThorchainEndpoint`
 *
 * @returns `PoolAddress`
 */
export const toPoolAddress = ({ address, chain }: ThorchainEndpoint) => ({ address, chain } as PoolAddress)

/**
 * Helper to transform a list of `PoolAddresses` to a list of `PoolAddress`
 *
 * @param list - List of `PoolAddresses`
 *
 * @returns List of `PoolAddress`
 */
export const toPoolAddressList = (list: PoolAddresses[]) =>
  list.reduce((acc, current) => [...acc, current.addresses], [])

/**
 * Fetches `PoolAddresses` from Midgard API endpoint
 *
 * @param ip - `ThorchainEndpoint`
 *
 * @returns `PoolAddresses` | Error
 */
export const getPoolAddress = async (ip: string): Promise<PoolAddresses> => {
  const endpoint = poolAddressEndpoint(ip)
  const result = await axios.get<ThorchainEndpoints>(endpoint, {
    headers: getDefaultHeader()
  })
  if (!result) {
    return Promise.reject(new Error(`Could not load pool addresses from IP ${ip}`))
  }
  const endpoints = result?.data?.current
  if (!endpoints) {
    return Promise.reject(new Error(`No pool addresses available at ${ip}`))
  }
  const addresses: PoolAddress[] = endpoints.map(toPoolAddress).sort(sortPoolAddressByChain)
  return { ip, addresses }
}

/**
 * Fetches list of IPs from seed endpoint
 *
 * @param network - Network
 *
 * @returns List of nodes (IPs) | Error
 */
export const getIpList = async (network: Network) => {
  const endpoint = seedEndpoint(network)
  const response = await axios.get<string[]>(endpoint, { headers: getDefaultHeader() })
  if (!response) {
    return Promise.reject(new Error(`Could not load IP list from ${endpoint}`))
  }

  if (response?.data?.length) {
    return response.data
  } else {
    return Promise.reject(new Error(`Empty IP list: ${response}`))
  }
}

/**
 * Helper to compare all pool addresses and chains in a list of `PoolAddresses`
 *
 * @param list - List of `PoolAddresses`
 *
 * @returns True if all addresses are equal, false if not
 */
export const comparePoolAddresses = (list: PoolAddresses[]): boolean => {
  // get pool addresses
  const addressList = toPoolAddressList(list)
  // remove one entry to compare it with all other at next step
  const addressesToCompare = addressList.pop()
  if (addressesToCompare) {
    return addressList.reduce(
      (acc: boolean, addresses: PoolAddress[]) => (acc ? comparePoolAddressLists(addressesToCompare, addresses) : acc),
      true
    )
  }
  return false
}

/**
 * Runs computation to get consensus of pool addresses
 */
const runPoolConsensus = async ({
  ipList,
  ipListResult = [],
  faultTolerance,
  poolAddresses
}: {
  ipList: IPList
  ipListResult?: IPList
  poolAddresses: PoolAddresses[]
  faultTolerance: number
}): Promise<IPList> => {
  const checkList = ipList.splice(0, faultTolerance)
  // Prepare requests for pool addresses
  const poolRequests = checkList.map((ip: IP) => getPoolAddress(ip))
  try {
    // Get all results, but ignoring failures
    const settledResult: PromiseSettledResult<PoolAddresses>[] = await Promise.allSettled(poolRequests)

    // to filter out valid (not failing) results
    const poolAddressesLoaded = settledResult.reduce((acc: PoolAddresses[], result) => {
      if (result.status === 'fulfilled') {
        acc.push(result.value)
      }
      return acc
    }, [])

    // pool addresses to check = prev. + current pool addresses
    const poolAddressesToCheck = poolAddresses.concat(poolAddressesLoaded)
    // compare all pool addresses we have loaded so far
    // if all are the same (valid), store its IPs in `ipListResult`
    // if not the same, just ignore it
    const validPoolAddresses = comparePoolAddresses(poolAddressesToCheck)

    if (validPoolAddresses) {
      ipListResult = ipListResult.concat(poolAddressesLoaded.map((value) => value.ip))
    }

    // Don't we have consensus?
    if (ipListResult.length < faultTolerance) {
      // If not, try to get consensus with remaing IPs in `ipList`
      if (ipList.length) {
        // next pool addresses to check (ignore current loaded addresses if they are not valid)
        const nextPoolAddresses = validPoolAddresses ? poolAddressesToCheck : poolAddresses
        return runPoolConsensus({ ipList, ipListResult, faultTolerance, poolAddresses: nextPoolAddresses })
      }
      // We don't have any nodes to check
      return Promise.reject(new Error(`Consensus failed - No other IPs to check`))
    }
    // We got consensus finally
    return Promise.resolve(ipListResult)
  } catch (e) {
    return Promise.reject(e)
  }
}

/**
 * Returns list of pool addresses in case of consensus
 *
 * @param list - List of nodes (IPs)
 *
 * @returns List of valid nodes (IPs)
 */
export const getPoolConsensus = async (ipList: IPList): Promise<IPList> => {
  // If we have one node, we are (mostly) about to start a (test|main)net
  if (ipList.length === 1) {
    return Promise.resolve(ipList)
  }
  // pool addresses based on fault tolerance
  const faultTolerance = getFaultTolerance(ipList.length)

  // Run another computation to get consensus
  const next = (ipList: IPList) => {
    // Run another check if possible
    if (poolChecks < MAX_POOL_CHECKS) {
      // count checks
      poolChecks++
      // Copy + shuffle list
      const list = _.shuffle([...ipList])
      return getPoolConsensus(list)
    } else {
      // or throw an error finally
      return Promise.reject(
        new Error(`Unable to reach consensus for pool addresses after ${MAX_POOL_CHECKS} attempt. Try it later again.`)
      )
    }
  }

  try {
    // Copy + shuffle list
    const list = _.shuffle([...ipList])
    return await runPoolConsensus({ ipList: list, faultTolerance, poolAddresses: [] })
  } catch (e) {
    // whenever it failed, run it again
    return next(ipList)
  }
}

/**
 * Returns a random IP from cache
 *
 * @throws Error - if cached list is empty. It should never happen (in theory...)
 */
export const cachedBaseUrl = (): string => {
  if (!cachedIpList?.length) {
    throw new Error(`Can't get baseUrl from cached list.`)
  }

  // we already shuffled the list before storing in `cachedIpList`,
  // so we just grab first item to return an random IP
  return midgartBaseUrl(cachedIpList[0])
}

/**
 * Key to store timestamp of last check
 */
export const TC_BYZANTINE_TIMESTAMP_KEY = 'tc-byzantine-ts'

const getBaseUrl = async (network: Network, timestamp: number): Promise<string> => {
  store.set(TC_BYZANTINE_TIMESTAMP_KEY, timestamp)
  // empty chached List
  cachedIpList = []
  // reset counter
  poolChecks = 0
  try {
    // load ip list
    const list: IPList = await getIpList(network)
    // One node means (in most cases) start of (test|chaos|main)net and we don't need to check consensus
    if (list.length === 1) {
      cachedIpList = list
    } else {
      // shuffled list to get more randomness
      const shuffledList = _.shuffle(list)
      // check consensus to store valid Ip's into cachedIpList
      cachedIpList = await getPoolConsensus(shuffledList)
    }
    return cachedBaseUrl()
  } catch (error) {
    return Promise.reject(error)
  }
}

/**
 * Returns current baseUrl of Midgard API of an active and proofed node
 *
 * @param network - Network (default 'testnet')
 * @param noCache - Flag to get baseUrl  from cache or not - default: false
 *
 * @returns Base url of Midgard. It might be a memorized value of a previous (valid) check if available
 */
export const baseUrl = async (network: Network = 'testnet', noCache = false): Promise<string> => {
  const now = new Date().getTime()
  const timestamp = store.get(TC_BYZANTINE_TIMESTAMP_KEY, now)
  // min. 1 hour for caching
  const minTimeDiff = 60 * 60 * 1000

  const newCheck = noCache || now - timestamp >= minTimeDiff
  if (newCheck || !cachedIpList?.length) {
    return getBaseUrl(network, now)
  }

  try {
    const value = cachedBaseUrl()
    return Promise.resolve(value)
  } catch (e) {
    return Promise.reject(e)
  }
}
