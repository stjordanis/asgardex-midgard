import { baseUrl } from '../src/byzantine'

const example = async () => {
  const url = await baseUrl('testnet', true).catch(console.error)
  console.log('testnet:', url)
}

example()
