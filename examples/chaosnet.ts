import { baseUrl } from '../src/byzantine'

const example = async () => {
  const url = await baseUrl('chaosnet', true).catch(console.error)
  console.log('chaosnet:', url)
}

example()
