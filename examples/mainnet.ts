import { baseUrl } from '../src/byzantine'

const example = async () => {
  const url = await baseUrl('mainnet', true).catch(console.error)
  console.log('mainnet:', url)
}

example()
